﻿using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace SendSangvinkerInvitations
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = ParseCalender(Data.Calender);

            foreach (var receiver in Data.Emails)
            {
                var calendars = new Dictionary<Team, Calendar>();
                foreach(Team team in Enum.GetValues(typeof(Team)))
                {
                    calendars.Add(team, new Calendar());
                }

                foreach (var match in input.Where(i => i.Players.Contains(receiver.Value.Name)))
                {
                    var @event = CreateEvent(match);
                    calendars[match.Team].Events.Add(@event);
                    calendars[match.Team].AddProperty(new CalendarProperty("X-WR-CALNAME", match.Team.ToString()));
                    calendars[match.Team].Method = "PUBLISH";
                }

                foreach (var calendar in calendars.Where(c => c.Value.Events.Any()))
                {
                    if (receiver.Value.OneEmailPerEvent)
                    {
                        foreach(var @event in calendar.Value.Events)
                        {
                            var newCalendar = new Calendar();
                            newCalendar.Events.Add(@event);
                            newCalendar.AddProperty(new CalendarProperty("X-WR-CALNAME", calendar.Key.ToString()));
                            newCalendar.Method = "REQUEST";
                            SendCalendar(receiver, calendar.Key, newCalendar);
                        }
                    }
                    else
                        SendCalendar(receiver, calendar.Key, calendar.Value);
                }
            }
        }

        private static void SendCalendar(KeyValuePair<string, (string Name, bool OneMailPerEvent)> receiver, Team team, Calendar calendar)
        {
            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);

            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(receiver.Key, receiver.Value.Name));
            message.From = new MailAddress(team + "@gmail.com", team.ToString());
            message.Subject = "Kalender för ligaspel i " + team;
            message.Body = $@"Hej {receiver.Value.Name}!
Bifogat finns en kalenderfil för ditt spel i {team} den kommande säsongen. Lägg till i din kalender genom att öppna filen. Testat med Outlook och Gmail. {(receiver.Value.OneMailPerEvent ? "" : "I gmail, tänk på att öppna filen och välj lägg till alla, om du väljer lägg till i kalendern direkt i kalendern läggs bara den första matchen till.")} Användning i andra kalenderprogram sker på egen risk.

Med vänliga hälsningar
{team}

Lycka till!";
            message.IsBodyHtml = false;
            var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
            MemoryStream ms = new MemoryStream(bytesCalendar);
            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, "event.ics", "text/calendar");
            message.Attachments.Add(attachment);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(team + "@gmail.com", Data.SmtpPassword)
            };

            smtp.Send(message);
        }

        private static IEnumerable<MatchData> ParseCalender(string calender)
        {
            var lines = calender.Split("\r\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                var match = new MatchData();

                var data = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (data.Length > 1)
                {
                    var date = DateTime.Parse(data[0] + ", 2020");
                    match.Date = date.AddHours(18).AddMinutes(30);
                    match.Team = (Team)Enum.Parse(typeof(Team), data[1], true);
                    match.Players = new[] { data[2], data[3], data[4], data[5] };
                    match.Opponent = data.Length > 6 ? string.Join(' ', data.Skip(6)) : null;
                    yield return match;
                }
            }
        }

        internal static CalendarEvent CreateEvent(MatchData match)
        {
            var e = new CalendarEvent
            {
                Start = new CalDateTime(match.Date),
                End = new CalDateTime(match.Date.AddHours(4)),
                Location = "S:t Erik, Kronobergsgatan 12 Stockholm",
                Description = $@"Bridgespel i ligan.
Lag {match.Team}.
Spelare denna gång preliminärt {string.Join(", ", match.Players)}. För aktuell laguppställning, se spelchemat på https://docs.google.com/spreadsheets/d/1uIFRCboIyU9mOFyRqLBfQZXfTOJYoZLO5I1FjVDuwtk/edit?usp=sharing.
Länk till budgivningsdokument: https://docs.google.com/document/d/16vNtCUQx7aLdF3rDtxYlMFRK1FuOP0ADiFZb_sYg_j8.
Nuvarande ställning {(match.Opponent == null ? Data.TeamInfo[match.Team].SlutspelResults : Data.TeamInfo[match.Team].GrundspelResults)}",
                Summary = "Bridgespel i S:t Eriks liga",
                Organizer = new Organizer(match.Team + "@gmail.com") { CommonName = match.Team.ToString() },
                
            };
            e.Alarms.Add(new Alarm
            {
                Summary = "Bridgespel i S:t Eriks liga",
                Trigger = new Trigger(TimeSpan.FromMinutes(-120)),
                Action = AlarmAction.Display
            });
            if (match.Opponent != null)
                e.Description += $"\r\nMotståndare denna gång är {match.Opponent}";

            e.Description += "\r\n\r\nTänk på att vara på plats i god tid (minst 15 minuter) innan spelstart.\r\n\r\nLycka till!";

            return e;
        }
    }
}
