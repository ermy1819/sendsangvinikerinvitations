﻿using System;

namespace SendSangvinkerInvitations
{
    internal class MatchData
    {
        public DateTime Date { get; internal set; }
        public Team Team { get; internal set; }
        public string[] Players { get; internal set; }
        public string Opponent { get; internal set; }
    }
}