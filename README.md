# SendSangvinkerInvitations

Built and run using Visual Studio 2019.

To use, edit Data.cs. Select the columns from the gdoc schedule for data, team, players and opponent. No other columns and not including the header row. Copy it and paste as the value for Calendar. Add emial-addreses for each player that want to receive invitations. Also add if they want one email per event, or one email per team. Lastly add the password for the smtp server (the password for the gmail accounts used). This is a public repository so I won't leave the password checked in.

Differences between one email per event or one email per team:

Outlook:

One email per event:

- Many mails to click through to add to calendar.
- Events appear in your standard calendar

One email per team:

- Only two emails to open attachements from.
- Each team will be a new calendar in outlook. Might be annoying but might also be good to be able to filter in or out as desired.

Gmail:

One email per event:

- Many mails to click through to add to calendar.
- Events can be added to calendar directly from email.

One email per team:

- Only two emails to open attachements from. Open the attachement and chose to add all events. Added to phones calendar as usual.
- Gmail will display only the first event. If it is added directly from the email, only the first event is added. Don't do this, open the ics file and let your phone handle it.
