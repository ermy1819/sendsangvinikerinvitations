﻿namespace SendSangvinkerInvitations
{
    internal class TeamInfo
    {
        internal string GrundspelResults { get; } 
        internal string Email { get; }
        internal string SlutspelResults { get; }

        public TeamInfo(string grundspelResults, string email, string slutspelResults)
        {
            GrundspelResults = grundspelResults;
            Email = email;
            SlutspelResults = slutspelResults;
        }
    }
}