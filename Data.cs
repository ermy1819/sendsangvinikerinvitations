﻿using System.Collections.Generic;
using System.Security;

namespace SendSangvinkerInvitations
{
    internal static class Data
    {
        internal static Dictionary<string, (string Name, bool OneEmailPerEvent)> Emails { get; }  = new Dictionary<string, (string, bool)>
        {
        };

        internal static string Calender => @"12-aug	Flegmatikerna	Erik	Maria	Mikaela	Tobias	Next time
13-aug Sangvinikerna   Jan Joakim  Klara Magnus  Nedlich
19-aug Flegmatikerna   Mikaela Calle   Joakim Magnus  TummenUpp
20-aug Sangvinikerna   Erik Maria   Calle Joakim  Wiklander
26-aug Flegmatikerna   Magnus Nillan  Joakim Calle   Hårda Bud
27-aug Sangvinikerna   Jan Nillan  Erik Mikaela Armstrong
02-sep Flegmatikerna   Magnus Nillan  Erik Joakim  Almgrens champions
03-sep Sangvinikerna   Magnus Nillan  Maria Mikaela Mosters vänner
09-sep Flegmatikerna   Klara Tobias  Magnus Nillan  Bridgeblandning
10-sep Sangvinikerna   Mikaela Joakim  Nillan Jan Sjöbohm
16-sep Flegmatikerna   Magnus Nillan  Maria Erik    Bustag
17-sep Sangvinikerna   Magnus Nillan  Calle Mikaela Årorna
23-sep Flegmatikerna   Joakim Maria   Calle Klara   Makkabi-Nytt
24-sep Sangvinikerna   Jan Magnus  Erik Calle   Bjälklaget
30-sep Flegmatikerna   Calle Joakim  Magnus Tobias  Ace of Spades
01-Oct Sangvinikerna   Maria Mikaela Calle Erik    Per Aspera
07-Oct Flegmatikerna   Calle Mikaela Joakim Maria   Kortoxen
08-Oct Sangvinikerna   Jan Magnus  Calle Erik    Sverik
14-Oct Flegmatikerna   Klara Mikaela Maria Joakim	
15-Oct Sangvinikerna   Calle Erik    Magnus Joakim	
21-Oct Flegmatikerna   Jan Nillan  Calle Mikaela	
22-Oct Sangvinikerna   Joakim Nillan  Erik Maria	
28-Oct Flegmatikerna   Erik Maria   Tobias Magnus	
29-Oct Sangvinikerna   Joakim Mikaela Klara Maria	
04-nov Flegmatikerna   Joakim Calle   Maria Mikaela	
05-nov Sangvinikerna   Calle Erik    Jan Magnus	
11-nov Flegmatikerna   Nillan Joakim  Erik Mikaela	
12-nov Sangvinikerna   Joakim Nillan  Maria Klara	
18-nov Flegmatikerna   Erik Maria   Joakim Tobias	
19-nov Sangvinikerna   Jan Magnus  Calle Mikaela	
25-nov Flegmatikerna   Klara Calle   Joakim Magnus	
26-nov Sangvinikerna   Erik Maria   Calle Mikaela	
02-dec Flegmatikerna   Jan Magnus  Erik Mikaela	
03-dec Sangvinikerna   Magnus Mikaela Erik Maria	
09-dec Flegmatikerna   Tobias Calle   Klara Maria	
10-dec Sangvinikerna   Calle Mikaela Erik Maria	";

        internal static Dictionary<Team, TeamInfo> TeamInfo { get; } = new Dictionary<Team, TeamInfo>
        {
            { Team.Flegmatikerna, new TeamInfo("https://www.sterik.se/Portals/48/docs/files/div4mndag.htm", "flegmatikerna@gmail.com", "https://www.sterik.se/Portals/48/docs/files/div3kvalm.htm eller https://www.sterik.se/Portals/48/docs/files/div4kvalm.htm") },
            { Team.Sangvinikerna, new TeamInfo("https://www.sterik.se/Portals/48/docs/files/div3tisdag.htm", "flegmatikerna@gmail.com", "https://www.sterik.se/Portals/48/docs/files/div2kvalti.htm eller https://www.sterik.se/Portals/48/docs/files/div3kvalti.htm") },
            { Team.Kolerikerna, new TeamInfo("", "kolerikerna@gmail.com", "") },
            { Team.Melankolikerna, new TeamInfo("", "melankolikerna@gmail.com", "") },
        };

        internal static string SmtpPassword => "xxxxxxxx";
    }
}
